
# README

There is a very good introduction [here](https://roelandtn.frama.io/post/how-to-create-a-docker-image/).

A short recap of how to create a new docker image:

```
systemctl start docker.service

docker build - < Dockerfile -t zauster/r-blogdown:0.1 -t zauster/r-blogdown:latest

docker image ls

docker login # keepassx
docker push zauster/r-blogdown:0.1
docker push zauster/r-blogdown:latest
```
